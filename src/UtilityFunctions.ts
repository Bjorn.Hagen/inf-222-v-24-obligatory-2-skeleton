import { EvaluatedResult } from "./evaluateVisitor";
import {
    Assignment,
    Additive,
    Identifier,
    Multiplicative,
    MeasuredNumber,
    AstNode,
    PhysicalUnitEnum,
    PhysicalUnit,
    GroupExpr,
  } from "./types";

export class UtilityFunctions {



    static convertToSmallestUnit(measuredNumber: MeasuredNumber) {

    switch(measuredNumber.unit.kind) {

        case PhysicalUnitEnum.Mass: measuredNumber = convertMassUnit(measuredNumber);
        case PhysicalUnitEnum.Distance: measuredNumber = convertDistanceUnit(measuredNumber);
        case PhysicalUnitEnum.Time: measuredNumber = convertTimeUnit(measuredNumber);
        case PhysicalUnitEnum.Velocity: measuredNumber = convertVelocityUnit(measuredNumber);
    return measuredNumber;
    }

}
}

function convertTimeUnit(measuredNumber: MeasuredNumber): MeasuredNumber {
    if (measuredNumber.unit.value === "s") {return measuredNumber;}
    else if (measuredNumber.unit.value === "min") {measuredNumber.numericalValue = measuredNumber.numericalValue * 60; measuredNumber.unit.value = "s";}
    else if (measuredNumber.unit.value === "h") {measuredNumber.numericalValue = measuredNumber.numericalValue * 3600; measuredNumber.unit.value = "s";}
    return measuredNumber;
}

function convertDistanceUnit(measuredNumber: MeasuredNumber): MeasuredNumber {
    if (measuredNumber.unit.value === "m") {return measuredNumber;}
    else if (measuredNumber.unit.value === "km") {measuredNumber.numericalValue = measuredNumber.numericalValue * 1000; measuredNumber.unit.value = "m";}
    return measuredNumber;

}

function convertVelocityUnit(measuredNumber: MeasuredNumber): MeasuredNumber {
    if (measuredNumber.unit.value === "m/s") {return measuredNumber;}
    else if (measuredNumber.unit.value === "km/min") {measuredNumber.numericalValue = measuredNumber.numericalValue * 16.667; measuredNumber.unit.value = "m/s";}
    else if (measuredNumber.unit.value === "m/h") {measuredNumber.numericalValue = measuredNumber.numericalValue * 0.0002778; measuredNumber.unit.value = "m/s";}
    else if (measuredNumber.unit.value === "km/s") {measuredNumber.numericalValue = measuredNumber.numericalValue * 1000; measuredNumber.unit.value = "m/s";}
    else if (measuredNumber.unit.value === "m/min") {measuredNumber.numericalValue = measuredNumber.numericalValue * 0.01667; measuredNumber.unit.value = "m/s";}
    else if (measuredNumber.unit.value === "km/h") {measuredNumber.numericalValue = measuredNumber.numericalValue * 0.2778; measuredNumber.unit.value = "m/s";}
    
    return measuredNumber;
}
function convertMassUnit(measuredNumber: MeasuredNumber): MeasuredNumber {
    if (measuredNumber.unit.value === "g") {return measuredNumber;}
    else if (measuredNumber.unit.value === "kg") {measuredNumber.numericalValue = measuredNumber.numericalValue * 1000; measuredNumber.unit.value = "g";}
    return measuredNumber;
}
